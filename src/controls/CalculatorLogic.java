package controls;

import java.math.BigDecimal;

public class CalculatorLogic {
	private static final int DEFAULT_PRECISION = 5;
	private double memory, actualCount, totalCount, signTerm;
	private Operation lastOperation;
	private int precision;

	public CalculatorLogic() {
		memory = actualCount = totalCount = 0;
		signTerm = 1;
		lastOperation = Operation.NONE;
		setPrecision(DEFAULT_PRECISION);		
	}
	
	public CalculatorLogic(double total) {
		memory = actualCount = 0;
		signTerm = 1;
		lastOperation = Operation.NONE;
		totalCount = total;
	}

	public double performOperation(double number, Operation operation) {

		switch (operation) {
			case CHANGE_SIGN:
				number *= -1.0;
				return number;
			case PERCENTAGE:
				number = actualCount * number / 100.0;
				return number;
			case FACTORIAL:
				number = factorial((int) number);
				return number;

		}

		switch (lastOperation) {
		case SUM:
			totalCount += actualCount*signTerm;
			signTerm = 1;
			actualCount = number;
			break;
		case SUBSTRACTION:
			totalCount += actualCount*signTerm;
			signTerm = -1;
			actualCount = number;
			break;
		case MULTIPLICATION:
			actualCount = multiplication(actualCount, number);
			break;
		case DIVISION:
			actualCount = division(actualCount, number);
			break;
		case POWER:
			actualCount = power(actualCount, number);
			break;
		case ROOT:
			actualCount = root(actualCount, number);
			break;
		case EQUALS:
			totalCount = 0;
			actualCount = number;
			signTerm = 1;
			break;
		case NONE:
			actualCount = number;
			
		}
		lastOperation = operation;
		if (operation == Operation.EQUALS) {
			totalCount += actualCount*signTerm;
			return getValue(totalCount);
		}
		
		return getValue(actualCount);
	}

	public void resetCount() {
		actualCount = 0;
		totalCount = 0;
	}

	public void setMemory(double number) {
		memory = number;
	}

	public double getMemory() {
		return memory;
	}

	public void sumMemory(double number) {
		memory += number;
	}

	public void substractMemory(double number) {
		memory -= number;
	}
	
	public void cleanMemory() {
		setMemory(0);
	}

	private int factorial(int n) {
		if (n <= 1)
			return n;
		return n * factorial(n - 1);
	}

	private double additon(double firstNumber, double secondNumber) {
		return firstNumber + secondNumber;
	}

	private double subtraction(double minuend, double subtracting) {
		return minuend - subtracting;
	}

	private double multiplication(double firstFactor, double secondFactor) {
		return firstFactor * secondFactor;
	}

	private double division(double firstNumber, double secondNumber) {
		if (secondNumber == 0)
			throw new ArithmeticException("Error: No se puede dividir por cero.");
		return firstNumber / secondNumber;
	}

	private double power(double base, double exponent) {
		return Math.pow(base, exponent);
	}

	private double root(double firstNumber, double secondNumber) {
		return Math.pow(secondNumber, 1 / firstNumber);
	}

	public double changeSign(double number) {
		return number * -1;
	}
	
	public void changeLastOperation(Operation operation) {
		lastOperation = operation;
	}
	
	public double getResultado() {
		return totalCount;
	}
	
	public Operation getLastOperation() {
		return lastOperation;
	}
	
	private double getValue(double value) {
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(precision, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

}
