package controls;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculatorLogicTest {

	@Test
	void performOperationTest1() {
		CalculatorLogic calc = new CalculatorLogic();
	
		calc.performOperation(4, Operation.SUM);
		calc.performOperation(6, Operation.MULTIPLICATION);
		calc.performOperation(3, Operation.SUBSTRACTION);
		calc.performOperation(6, Operation.DIVISION);
		calc.performOperation(3, Operation.EQUALS);
		
		assertEquals(20, calc.getResultado());
	}
	
	@Test
	void performOperationTest2() {
		CalculatorLogic calc = new CalculatorLogic();
		double resultado;
		calc.performOperation(20, Operation.SUM);
		resultado = calc.performOperation(20, Operation.PERCENTAGE);
		calc.performOperation(resultado, Operation.EQUALS);
		
		assertEquals(24, calc.getResultado());
	}
	
	@Test
	void performOperationTest3() {
		CalculatorLogic calc = new CalculatorLogic();
		double resultado;
	
		calc.performOperation(24, Operation.DIVISION);
		resultado = calc.performOperation(3, Operation.FACTORIAL);
		calc.performOperation(resultado, Operation.EQUALS);
		
		assertEquals(4, calc.getResultado());	
	}
	
	@Test
	void performOperationTest4() {
		CalculatorLogic calc = new CalculatorLogic();

		calc.performOperation(4, Operation.POWER);
		calc.performOperation(2, Operation.EQUALS);
		
		assertEquals(16, calc.getResultado());
	}
	
	@Test
	void performOperationTest5() {
		CalculatorLogic calc = new CalculatorLogic();
		
		calc.performOperation(4, Operation.ROOT);
		calc.performOperation(16, Operation.EQUALS);
		
		assertEquals(2, calc.getResultado());
	}
	
	@Test
	void performOperationTest6() {
		CalculatorLogic calc = new CalculatorLogic();

		calc.performOperation(2, Operation.DIVISION);
		
		try{
			calc.performOperation(0, Operation.EQUALS);
			assertTrue(false);
		}catch(Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	void setMemoryTest() {
		CalculatorLogic calc = new CalculatorLogic();
		calc.setMemory(8);
		
		assertEquals(8, calc.getMemory());
	}
	
	@Test
	void substractMemoryTest() {
		CalculatorLogic calc = new CalculatorLogic();
		
		calc.setMemory(8);
		calc.substractMemory(3);
	
		assertEquals(5, calc.getMemory());
	}
	
	@Test
	void sumMemoryTest() {
		CalculatorLogic calc = new CalculatorLogic();
		calc.setMemory(8);
	
		calc.sumMemory(5);
		assertEquals(13, calc.getMemory());
	}
	
	@Test
	void cleanMemoryTest() {
		CalculatorLogic calc = new CalculatorLogic();
		calc.setMemory(8);
	
		calc.cleanMemory();
		assertEquals(0, calc.getMemory());
	}
}
