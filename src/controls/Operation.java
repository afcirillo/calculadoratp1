package controls;

public enum Operation {
	SUM, 
	SUBSTRACTION, 
	MULTIPLICATION, 
	DIVISION,
	POWER,
	ROOT,
	CHANGE_SIGN,
	PERCENTAGE,
	FACTORIAL,
	EQUALS,
	NONE
}
