package Interface;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;
import controls.CalculatorLogic;
import controls.Operation;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

public class Calculator {

	private static final int NUMERIC_BUTTON_QUANTITY = 10;
	private static final int OPERATOR_BUTTON_QUANTITY = 6;
	private static final int OTHER_OPERATOR_BUTTON_QUANTITY = 2;

	enum Status {
		SHOW_RESULT, SHOW_ERROR, ENTERING_NUMBER, ENTERING_OPERATOR, OTHER_OPERATOR
	};

	public Status getStatusCalc() {
		return statusCalc;
	}

	public void setStatusCalc(Status status) {
		this.statusCalc = status;
	}

	private Status statusCalc;
	private CalculatorLogic calc;

	protected Shell shell;

	private Button[] numericButton;
	private Button[] operatorButton;
	private Button[] otherOperatorButton;
	private Button btnBack;
	private Button btnC;
	private Button btnCE;
	private Button btnEquals;
	private Button btnPoint;
	private Button btnMemoryClean;
	private Button btnMemoryReturn;
	private Button btnMemorySet;
	private Button btnMemorySum;
	private Button btnMemorySubstract;
	private Button btnSign;

	Button sendSavedHistory;
	private List lstHistory;

	Label lblPreview;
	Label lblView;

	private List lstMemory;

	public static void main(String[] args) {
		try {
			Calculator window = new Calculator();

			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void open() {
		Display display = Display.getDefault();
		createContents();
		createEvent();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	protected void createContents() {
		setStatusCalc(Status.ENTERING_NUMBER);

		shell = new Shell(SWT.MIN);
		shell.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		shell.setSize(485, 485);
		shell.setText("Calculadora");
		shell.setLayout(null);
		shell.setBackground(new Color(null, 225, 225, 225));
		numericButton = new Button[NUMERIC_BUTTON_QUANTITY];
		operatorButton = new Button[OPERATOR_BUTTON_QUANTITY];
		otherOperatorButton = new Button[OTHER_OPERATOR_BUTTON_QUANTITY];

		btnMemoryClean = new Button(shell, SWT.NONE);
		btnMemoryClean.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnMemoryClean.setText("MC");
		btnMemoryClean.setBounds(10, 127, 50, 48);

		btnMemoryReturn = new Button(shell, SWT.NONE);
		btnMemoryReturn.setText("MR");
		btnMemoryReturn.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnMemoryReturn.setBounds(66, 127, 50, 48);

		btnMemorySet = new Button(shell, SWT.NONE);
		btnMemorySet.setText("MS");
		btnMemorySet.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnMemorySet.setBounds(122, 127, 50, 48);

		btnMemorySum = new Button(shell, SWT.NONE);
		btnMemorySum.setText("M+");
		btnMemorySum.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnMemorySum.setBounds(178, 127, 50, 48);

		btnMemorySubstract = new Button(shell, SWT.NONE);
		btnMemorySubstract.setText("M-");
		btnMemorySubstract.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnMemorySubstract.setBounds(234, 127, 50, 48);

		btnCE = new Button(shell, SWT.NONE);
		btnCE.setText("CE");
		btnCE.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnCE.setBackground(new Color(null, 226, 107, 0));
		btnCE.setBounds(10, 187, 50, 48);

		btnC = new Button(shell, SWT.NONE);
		btnC.setText("C");
		btnC.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnC.setBackground(new Color(null, 255, 0, 0));
		btnC.setBounds(66, 187, 50, 48);

		btnBack = new Button(shell, SWT.NONE);
		btnBack.setImage(new Image(null, "src/img/btn_back.png"));
		btnBack.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnBack.setBounds(122, 187, 50, 48);

		for (int i = 0; i < NUMERIC_BUTTON_QUANTITY; i++) {
			numericButton[i] = new Button(shell, SWT.NONE);
			numericButton[i].setText(String.valueOf(i));
			numericButton[i].setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		}

		numericButton[0].setBounds(66, 403, 50, 48);
		numericButton[1].setBounds(10, 349, 50, 48);
		numericButton[2].setBounds(66, 349, 50, 48);
		numericButton[3].setBounds(122, 349, 50, 48);
		numericButton[4].setBounds(10, 295, 50, 48);
		numericButton[5].setBounds(66, 295, 50, 48);
		numericButton[6].setBounds(122, 295, 50, 48);
		numericButton[7].setBounds(10, 241, 50, 48);
		numericButton[8].setBounds(66, 241, 50, 48);
		numericButton[9].setBounds(122, 241, 50, 48);

		Operation[] operators = new Operation[OPERATOR_BUTTON_QUANTITY];
		operators[0] = Operation.SUM;
		operators[1] = Operation.SUBSTRACTION;
		operators[2] = Operation.MULTIPLICATION;
		operators[3] = Operation.DIVISION;
		operators[4] = Operation.POWER;
		operators[5] = Operation.ROOT;

		for (int i = 0; i < OPERATOR_BUTTON_QUANTITY; i++) {
			operatorButton[i] = new Button(shell, SWT.NONE);
			operatorButton[i].setData((Object) operators[i]);
			operatorButton[i].setText(getOperator(operators[i]));
			operatorButton[i].setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		}
		operatorButton[0].setBounds(178, 187, 50, 48);
		operatorButton[1].setBounds(178, 241, 50, 48);
		operatorButton[2].setBounds(178, 295, 50, 48);
		operatorButton[3].setBounds(178, 349, 50, 48);
		operatorButton[4].setBounds(234, 295, 50, 48);
		operatorButton[4].setImage(new Image(null, "src/img/btn_power.png"));
		operatorButton[5].setBounds(234, 349, 50, 48);
		operatorButton[5].setImage(new Image(null, "src/img/btn_root.png"));

		operators = new Operation[OTHER_OPERATOR_BUTTON_QUANTITY];
		operators[0] = Operation.PERCENTAGE;
		operators[1] = Operation.FACTORIAL;
		for (int i = 0; i < OTHER_OPERATOR_BUTTON_QUANTITY; i++) {
			otherOperatorButton[i] = new Button(shell, SWT.NONE);
			otherOperatorButton[i].setData((Object) operators[i]);
			otherOperatorButton[i].setText(getOperator(operators[i]));
			otherOperatorButton[i].setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		}
		otherOperatorButton[0].setBounds(234, 241, 50, 48);
		otherOperatorButton[1].setBounds(234, 187, 50, 48);
		otherOperatorButton[1].setText("n!");

		btnSign = new Button(shell, SWT.NONE);
		btnSign.setText("+/-");
		btnSign.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnSign.setBounds(10, 403, 50, 48);

		btnPoint = new Button(shell, SWT.NONE);
		btnPoint.setText(".");
		btnPoint.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnPoint.setBounds(122, 403, 50, 48);

		btnEquals = new Button(shell, SWT.NONE);
		btnEquals.setText("=");
		btnEquals.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.NORMAL));
		btnEquals.setBounds(178, 403, 106, 48);

		lblPreview = new Label(shell, SWT.RIGHT);
		lblPreview.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		lblPreview.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblPreview.setBounds(10, 10, 274, 28);
		lblPreview.setText("");

		lblView = new Label(shell, SWT.WRAP | SWT.RIGHT);
		lblView.setText("0");
		lblView.setFont(SWTResourceManager.getFont("Segoe UI", 20, SWT.NORMAL));
		lblView.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		lblView.setBounds(10, 36, 274, 74);

		Label label = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(10, 10, 274, 2);

		Label label_2 = new Label(shell, SWT.SEPARATOR | SWT.VERTICAL);
		label_2.setBounds(10, 10, 2, 100);

		TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
		tabFolder.setBounds(297, 10, 177, 441);

		TabItem tbtmHistorial = new TabItem(tabFolder, SWT.NONE);
		tbtmHistorial.setText("Historial");

		lstHistory = new List(tabFolder, SWT.BORDER);
		tbtmHistorial.setControl(lstHistory);
		lstHistory.setItems(new String[] {});

		TabItem tbtmMemoria = new TabItem(tabFolder, 0);
		tbtmMemoria.setText("Memoria");

		lstMemory = new List(tabFolder, SWT.BORDER);
		lstMemory.setItems(new String[] {});
		tbtmMemoria.setControl(lstMemory);

		calc = new CalculatorLogic();
	}

	void createEvent() {
		for (int i = 0; i < NUMERIC_BUTTON_QUANTITY; i++) {
			numericButton[i].addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					addInView(((Button) e.getSource()).getText());
				}
			});
		}
		for (int i = 0; i < OPERATOR_BUTTON_QUANTITY; i++) {
			operatorButton[i].addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					if (!addInPreview((Operation) ((Button) e.getSource()).getData()))
						return;
					if (getStatusCalc() != Status.ENTERING_OPERATOR)
						callOperation((Operation) ((Button) e.getSource()).getData());
				}
			});
		}

		for (int i = 0; i < OTHER_OPERATOR_BUTTON_QUANTITY; i++) {
			otherOperatorButton[i].addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					actionInError();
					if (!addInPreview((Operation) ((Button) e.getSource()).getData()))
						return;
					callOperation((Operation) ((Button) e.getSource()).getData());
					setStatusCalc(Status.OTHER_OPERATOR);
				}
			});

		}

		btnPoint.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				addInView(btnPoint.getText());
			}
		});

		btnSign.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				actionInError();
				if (getStatusCalc() == Status.SHOW_RESULT)
					resetPreview();
				callOperation(Operation.CHANGE_SIGN);
				setStatusCalc(Status.ENTERING_NUMBER);
			}
		});
		btnEquals.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				addInPreview(Operation.EQUALS);
				if (callOperation(Operation.EQUALS)) {
					lstHistory.add(lblPreview.getText() + lblView.getText(), 0);
					setStatusCalc(Status.SHOW_RESULT);
				}
			}
		});
		btnC.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setStatusCalc(Status.ENTERING_NUMBER);
				calc = new CalculatorLogic();
				resetView();
				resetPreview();
				resetHistoryList();
				resetMemoryList();
			}

		});
		btnCE.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setStatusCalc(Status.ENTERING_NUMBER);
				resetView();
			}
		});
		btnBack.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (lblView.getText().length() == 1) {
					setStatusCalc(Status.ENTERING_NUMBER);
					resetView();
					return;
				}

				if (lblView.getText().length() > 1)
					lblView.setText(lblView.getText().substring(0, lblView.getText().length() - 1));
				else
					resetView();
			}
		});
		btnMemorySet.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				actionInError();
				calc.setMemory(toDoble((lblView.getText().isEmpty()) ? "0" : lblView.getText()));
				lstMemory.add(formatNumber(calc.getMemory()), 0);
			}
		});
		btnMemorySum.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				actionInError();
				calc.sumMemory(toDoble(lblView.getText()));
				showMemoryStatus();
			}

		});
		btnMemorySubstract.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				actionInError();
				calc.substractMemory(toDoble(lblView.getText()));
				showMemoryStatus();
			}
		});
		btnMemoryClean.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				calc.cleanMemory();
				removeLstMemory();
			}
		});
		btnMemoryReturn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				if (lstMemory.getItemCount() == 0)
					return;

				setStatusCalc(Status.ENTERING_NUMBER);

				if (lstMemory.getSelectionCount() != 1) {
					lblView.setText(lstMemory.getItem(0));
					return;
				}
				lblView.setText(lstMemory.getSelection()[0]);

			}
		});
		lstHistory.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				if (lstHistory.getSelectionIndex() != -1) {
					String item = lstHistory.getItem(lstHistory.getSelectionIndex());

					lblPreview.setText(item.substring(0, item.indexOf(getOperator(Operation.EQUALS)) + 1));
					lblView.setText(item.substring(item.indexOf(getOperator(Operation.EQUALS)) + 1));
					calc = new CalculatorLogic();
					setStatusCalc(Status.SHOW_RESULT);
				}
			}

			@Override
			public void mouseDown(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseUp(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		lstMemory.addMouseListener(new MouseListener() {

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				if (lstMemory.getSelectionIndex() != -1) {
					String item = lstMemory.getItem(lstMemory.getSelectionIndex());
					lblView.setText(item);
				}
			}

			@Override
			public void mouseDown(MouseEvent arg0) {
				if (lstMemory.getSelectionIndex() != -1) {
					String item = lstMemory.getItem(lstMemory.getSelectionIndex());
					calc.setMemory(toDoble(item));
				}

			}

			@Override
			public void mouseUp(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	protected void addInView(String number) {
		if (getStatusCalc() != Status.ENTERING_NUMBER) {
			resetView();
			if (getStatusCalc() != Status.ENTERING_OPERATOR)
				resetPreview();
			setStatusCalc(Status.ENTERING_NUMBER);
		}
		if (lblView.getText().equals("0") && !number.equals("."))
			lblView.setText("");

		if (number.equals(".") && lblView.getText().indexOf(".") >= 0)
			return;

		lblView.setText(lblView.getText() + number);

	}

	private boolean addInPreview(Operation operation) {
		String view = lblView.getText();
		switch (statusCalc) {
		case OTHER_OPERATOR:
			view = "";
			break;
		case ENTERING_OPERATOR:
			if (isOtherOperator(operation))
				return false;
			changeOperationSign(operation);
			return true;
		case SHOW_ERROR:
			resetPreview();
			break;
		case SHOW_RESULT:
			resetPreview();
		default:
			if (lblView.getText().isEmpty())
				resetView();

		}

		lblPreview.setText(lblPreview.getText() + view + getOperator(operation));
		return true;
	}

	private double toDoble(String text) {
		// TODO Auto-generated method stub
		try {
			return Double.parseDouble(text);
		} catch (Exception e) {

		}
		return 0;
	}

	private void resetPreview() {
		lblPreview.setText("");
	}

	private boolean callOperation(Operation operation) {
		try {
			double result = calc.performOperation(toDoble(lblView.getText()), operation);
			showResult(result);
			setStatusCalc(Status.ENTERING_OPERATOR);
			return true;
		} catch (ArithmeticException e) {
			showMessage(e.getMessage());
			resetView();
			setStatusCalc(Status.SHOW_ERROR);
			calc = new CalculatorLogic();
		}
		return false;

	}

	private void showResult(double result) {
		lblView.setText(formatNumber(result));
	}

	private void showMessage(String message) {
		lblPreview.setText(message);
	}

	private String getOperator(Operation operation) {
		switch (operation) {
		case SUM:
			return "+";
		case SUBSTRACTION:
			return "-";
		case MULTIPLICATION:
			return "*";
		case DIVISION:
			return "/";
		case POWER:
			return "^";
		case ROOT:
			return "V";
		case PERCENTAGE:
			return "%";
		case FACTORIAL:
			return "!";
		case EQUALS:
			return "=";
		default:
			return "";
		}
	}

	private String formatNumber(double number) {
		int intNumber = (int) number;
		if (intNumber == number)
			return String.valueOf(intNumber);
		return String.valueOf(number);
	}

	private void showMemoryStatus() {
		if (lstMemory.getItemCount() == 0) {
			lstMemory.add(formatNumber(calc.getMemory()));
			lstMemory.setSelection(0);
			return;
		}
		if (lstMemory.getSelectionCount() == 0) {
			lstMemory.setSelection(0);
		}
		int index = lstMemory.getSelectionIndex();
		lstMemory.setItem(index, formatNumber(calc.getMemory()));
	}

	private void resetView() {
		lblView.setText("0");
	}

	private void removeLstMemory() {
		lstMemory.removeAll();
	}

	private void changeOperationSign(Operation operation) {
		String preview = lblPreview.getText();
		String lastOperation = getOperator(calc.getLastOperation());
		preview = preview.substring(0, preview.lastIndexOf(lastOperation));
		lblPreview.setText(preview + getOperator(operation));
		calc.changeLastOperation(operation);
		setStatusCalc(Status.ENTERING_OPERATOR);
	}
	
	private boolean isOtherOperator(Operation operator) {
		for (int i = 0; i < OTHER_OPERATOR_BUTTON_QUANTITY; i++) {
			if (otherOperatorButton[i].getData() == operator)
				return true;
		}
		return false;
	}
	
	private boolean isOperator(Operation operator) {
		for (int i = 0; i < OPERATOR_BUTTON_QUANTITY; i++) {
			if (operatorButton[i].getData() == operator)
				return true;
		}
		return false;
	}

	private void actionInError() {
		if (statusCalc == Status.SHOW_ERROR)
			resetView();
	}

	private void resetMemoryList() {
		lstMemory.removeAll();
	}

	private void resetHistoryList() {
		lstHistory.removeAll();
	}

}
